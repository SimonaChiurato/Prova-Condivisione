# -*- coding: utf-8 -*-
"""
Created on Fri Oct 22 17:03:16 2021

@author: Simona
"""
import cherrypy
import json
import math
class Calculator(object):
    exposed=True #expose the method as an URL
    def __init__(self):
        pass
    
    @cherrypy.tools.json_out()
    def GET(self,*uri,**params):
        if len(uri)==1:  
            op = uri[0]
            listParam=[]
            result = None
            if op=="div":
                if len(params)==2:
                    for k in params:
                        # num=int()
                        listParam.append(params[k])
                    listParam=[int(i) for i in listParam]
                    if listParam[1]==0:
                        raise cherrypy.HTTPError(400, 'Is not possible divide a number for zero')
                    else:
                        result= str(listParam[0]/listParam[1])
                else:
                     raise cherrypy.HTTPError(400, 'In order to performe a division you have to write only two numbers')
            else:
                 if len(params)>=2:
                     for k in params:
                         # num=int()
                         listParam.append(params[k])
                     listParam=[int(i) for i in listParam]
                     if op == "add":
                        result= str(sum(listParam))
                     elif op == "sub":
                        result= str(listParam[0]-listParam[1])
                     elif op == "mul":
                        result= str(listParam[0]*listParam[1])    
            if op!="div" and op != "add" and op != "sub" and op != "mul":
                raise cherrypy.HTTPError(400, 'Wrong input as operation, you can write only "div", "add", "sub", "mul"')
            resultJSON ={
                "operation" : op,
                "parameters" : listParam,
                "result" : result
                } 
            return resultJSON
                    
    # def __init__(self):
    #     self
    def operation(self):
        if self.op == "add":
            return self.x1+self.x2
        elif self.op == "sub":
            return self.x1-self.x2
        elif self.op == "mul":
            return self.x1*self.x2
        elif self.op == "div":
            return self.x1/self.x2
    def add(self, a, b):
        return a+b
    def sub(self, a, b):
        return a-b
    def mul(self, a, b):
        return a*b
    def div(self, a, b):
        return a/b